<?php

namespace KCMB\AppBundle\Menu;

use Application\Sonata\UserBundle\Entity\User;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $user = null;
        $options = array_merge($options, ['navbar' => true]);
        $tokenStorage = $this->container->get('security.token_storage');
        $request = $tokenStorage;

        if ($request->getToken() !== null) {
            $user = $request->getToken()->getUser();
        }

        if (! $user instanceof User) {
            $menu = $this->anonymousMenu($factory, $options);
        } else {
            $menu = $this->userMenu($factory, $options);
        }

        return $menu;
    }

    public function anonymousMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        $menu
            ->addChild('Zaloguj się', [
                'route' => 'fos_user_security_login'
            ])
                ->setExtra('icon', 'fa fa-list')
            ->addChild('Zarejestruj się', [
                'route' => 'fos_user_registration_register'
            ])
        ;
        return $menu;
    }

    public function userMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu
            ->addChild('Mój profil', [
                'route' => 'kcmb_app_user_profile'
            ])
                ->setExtra('icon', 'fa fa-list');
        $menu['Mój profil']->addChild('Edytuj profil', [
            'route' => 'kcmb_app_user_edit'
        ]);
        $menu['Mój profil']->addChild('Wyloguj się', [
            'route' => 'fos_user_security_logout'
        ]);
        return $menu;
    }
}