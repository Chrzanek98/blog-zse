<?php

namespace KCMB\AppBundle\Migrations\Fixtures;

use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\UserBundle\Entity\User;
use KCMB\AppBundle\Migrations\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends AbstractFixture
{
    const NUM_USERS = 45;

    protected $users;
    /** @var  $manager ObjectManager */
    private $manager;

    public function __construct()
    {
        $this->users = array_map(function($item) {return 'user'.$item;}, array_merge([''], range(0, self::NUM_USERS)));
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $this->createAvatar();
        $this->createAdmin();
        $this->createBlogUsers();
    }

    private function createBlogUsers()
    {
        $manager = $this->manager;
        $faker = $this->getFaker();
        foreach ($this->users as $userX) {
           if($user = $this->createUser($userX, 'user')) {
                $user->setShortDescription($faker->text(400));
                $user->addRole('ROLE_USER');
                $manager->persist($user);
           }
        }
        $manager->flush();
    }

    private function createAdmin()
    {
        $manager = $this->getUserManager();

        $user = $this->createUser('admin', 'admin');
        $user->setFirstname('Admin');
        $user->setLastname('');
        $user->setEmail('chrzanek@local.com');
        $user->setEmailCanonical('chrzanek@local.com');
        $user->setSuperAdmin(true);
        $manager->updateUser($user);
    }

    protected function createAvatar()
    {
        $path = $this->container->get('file_locator')->locate('@KCMBAppBundle/Migrations/Fixtures/Files/sample_avatar.png');

        $media = new Media();

        $media->setBinaryContent($path);
        $media->setWidth(150);
        $media->setHeight(150);
        $media->setContext('defaults');
        $media->setProviderName('sonata.media.provider.image');
        $media->setOriginalName('sample_avatar');

        $this->manager->persist($media);
        $this->manager->flush();
    }

}