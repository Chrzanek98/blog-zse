<?php

namespace KCMB\AppBundle\Migrations\Fixtures;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Persistence\ObjectManager;
use KCMB\AppBundle\Entity\Post;
use KCMB\AppBundle\Migrations\AbstractFixture;

class LoadBlogData extends AbstractFixture
{
    const NUM_OF_POSTS = 35;
    /** @var  $manager ObjectManager */
    private $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->createPosts();
    }

    private function createPosts()
    {
        $faker = $this->getFaker();

        for ($i = 0; $i < self::NUM_OF_POSTS; $i++) {
            $post = new Post();
            $post
                ->setContent($faker->text(900))
                ->setTitle($faker->text(30))
                ->setHeaderImage($this->getSampleBanner())
            ;
            $this->manager->persist($post);
        }
        $this->manager->flush();
    }

    private function getSampleBanner()
    {
        $numOfImage = rand(1,4);
        if ($this->hasReference('sample_image_'.$numOfImage)) {
            return $this->getReference('sample_image_'.$numOfImage);
        } else {
            /** @var Media $media */
            $mediaManager = $this->getMediaManager();
            $media = $mediaManager->create();
            $media->setBinaryContent(__DIR__.'/Files/sample_image_'.$numOfImage.'.jpg');
            $media->setEnabled(true);
            $media->setName("Sample banner image ".$numOfImage);
            $media->setContext('banners');
            $media->setProviderName('sonata.media.provider.image');
            $mediaManager->save($media);

            $this->addReference('sample_image_'.$numOfImage, $media);
            return $media;
        }
    }
}