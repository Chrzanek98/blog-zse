<?php

namespace KCMB\AppBundle\Migrations;

use Application\Sonata\UserBundle\Entity\User;
use Sonata\UserBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\AbstractFixture as BaseAbstractFixture;
/**
 * Category fixtures loader.
 *
 * @author Hugo Briand <briand@ekino.com>
 * @author Sylvain Deloux <sylvain.deloux@ekino.com>
 */
abstract class AbstractFixture extends BaseAbstractFixture implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;


    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function createUser($username, $password, $male = true)
    {
        $manager = $this->getUserManager();

        if ($manager->findUserByUsername($username)) {
            return false;
        }

        $faker = $this->getFaker();
        /** @var User $user */
        $user = $manager->createUser();
        $user->setUsername($username);
        $user->setNickname($username);
        $user->setFirstName($faker->{$male?'firstNameMale':'firstNameFemale'});
        $user->setLastName($faker->lastName);
        $user->setDateOfBirth($faker->dateTimeBetween('-30 years','-13 years'));
        $user->setEmail($faker->safeEmail);
        $user->setPlainPassword($password);
        $user->setEnabled(true);

        $manager->updateUser($user);

        $this->addReference('user-'.$username, $user);

        return $user;
    }

    /**
     * @return \Faker\Generator
     */
    public function getFaker()
    {
        return $this->container->get('faker.generator');
    }


    /**
     * @return \FOS\UserBundle\Model\UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->container->get('fos_user.user_manager');
    }


    /**
     * @return \Sonata\MediaBundle\Model\MediaManagerInterface
     */
    public function getMediaManager()
    {
        return $this->container->get('sonata.media.manager.media');
    }
}
