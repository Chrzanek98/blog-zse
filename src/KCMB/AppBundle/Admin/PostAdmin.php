<?php

namespace  KCMB\AppBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PostAdmin extends AbstractAdmin
{
    public function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id', null, [
                'route' => [
                    'name' => 'edit'
                ]
            ])
            ->add('title')
        ;
    }

    public function configureFormFields(FormMapper $form)
    {
        $form
            ->add('title')
            ->add('content', CKEditorType::class)
            ->add('headerImage', MediaType::class, [
                'provider' => 'sonata.media.provider.image',
                'context' => 'avatars',
            ])
        ;
    }
}
