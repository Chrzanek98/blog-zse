<?php

namespace KCMB\AppBundle\Form\Handler;

use FOS\UserBundle\Model\UserInterface;
use Sonata\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RequestStack;

class EditProfileFormHandler
{
    /**
     * @var RequestStack
     */
    protected $request;
    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    public function __construct(RequestStack $request, UserManagerInterface $userManager)
    {
        $this->request = $request;
        $this->userManager = $userManager;
    }

    public function process(Form $form, UserInterface $user)
    {
        if ($this->request->getCurrentRequest()->getMethod() === "POST") {
            $form->handleRequest($this->request->getCurrentRequest());
            if ($form->isValid()) {
                $this->onSuccess($user);
                return true;
            }
        }

        return false;
    }

    protected function onSuccess(UserInterface $user)
    {
        $this->userManager->updateUser($user);
    }

}