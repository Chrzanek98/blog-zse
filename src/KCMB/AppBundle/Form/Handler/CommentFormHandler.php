<?php

namespace KCMB\AppBundle\Form\Handler;

use Doctrine\ORM\EntityManagerInterface;
use KCMB\AppBundle\Entity\Comment;
use KCMB\AppBundle\Entity\Post;

class CommentFormHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function process(Post $post, Comment $comment)
    {
        if ($post && $comment) {
            $post->addComment($comment);
            $comment->setPost($post);
            $this->onSuccess($post, $comment);
            return true;
        }
        return false;
    }

    protected function onSuccess(Post $post, Comment $comment)
    {
        $this->entityManager->persist($comment);
        $this->entityManager->persist($post);
        $this->entityManager->flush();
    }

}