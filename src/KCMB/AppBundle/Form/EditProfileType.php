<?php

namespace KCMB\AppBundle\Form;

use Application\Sonata\UserBundle\Entity\User;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $date = new \DateTime();

        $builder
            ->add('firstName', TextType::class, [
                    'label' => 'registration.label.firstName',
                    'required' => true,
                    'translation_domain' => 'messages'
            ])
            ->add('lastName', TextType::class, [
                'label' => 'registration.label.lastName',
                'required' => false,
            ])
            ->add('nickname', TextType::class, [
                'label' => 'registration.label.nickname',
                'required' => true
            ])
            ->add('email', EmailType::class, [
                'label' => 'registration.label.email',
                'required' => true
            ])
            ->add('dateOfBirth', DatePickerType::class, [
                'label' => 'registration.label.dateOfBirth',
                'years' => range(1950, $date->format('Y') - 12),
                'widget' => 'choice',
                'required' => true
            ])
            ->add('avatar', MediaType::class, [
                'label' => 'registration.label.avatar',
                'required' => false,
                'provider' => 'sonata.media.provider.image',
                'context' => 'avatars',
                'translation_domain' => 'messages',
            ])
            ->add('plainPassword', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'first_options' => [
                        'label' => 'registration.label.plainPassword.labelFirst',
                        'attr' => [
                            'placeholder' => 'registration.label.plainPassword.placeholder'
                        ],
                    ],
                    'second_options' => [
                        'label' => 'registration.label.plainPassword.labelRepeat',
                    ],
                    'invalid_message' => 'registration.label.plainPassword.mismatch',
                    'property_path' => 'plainPassword',
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $optionsResolver)
    {
        $optionsResolver->setDefaults([
            'translation_domain' => 'messages',
            'data_class' => User::class,
//            'empty_data' => User::class
        ]);
    }
}