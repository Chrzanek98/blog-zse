<?php

namespace KCMB\AppBundle\Form;

use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\IsTrue;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $date = new \DateTime();
        $media = new Media();
        $media->setContext('avatars');
        $media->setProviderName('sonata.media.provider.image');

        $builder
            ->add('firstName', TextType::class, [
                    'label' => 'registration.label.firstName',
                    'required' => true,
                    'translation_domain' => 'messages'
            ])
            ->add('lastName', TextType::class, [
                'label' => 'registration.label.lastName',
                'required' => false
            ])
            ->add('nickname', TextType::class, [
                'label' => 'registration.label.nickname',
                'required' => true
            ])
            ->add('email', EmailType::class, [
                'label' => 'registration.label.email',
                'required' => true
            ])
            ->add('dateOfBirth', DatePickerType::class, [
                'label' => 'registration.label.dateOfBirth',
                'years' => range(1950, $date->format('Y') - 12),
                'widget' => 'choice',
                'required' => true
            ])
            ->add('avatar', MediaType::class, [
                'label' => 'registration.label.avatar',
                'required' => false,
                'provider' => 'sonata.media.provider.image',
                'context' => 'avatars',
                'translation_domain' => 'messages',
            ])
            ->add('plainPassword', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'first_options' => [
                        'label' => 'registration.label.plainPassword.labelFirst',
                        'attr' => [
                            'placeholder' => 'registration.label.plainPassword.placeholder'
                        ],
                    ],
                    'second_options' => [
                        'label' => 'registration.label.plainPassword.labelRepeat',
                    ],
                    'invalid_message' => 'registration.label.plainPassword.mismatch',
                    'property_path' => 'plainPassword',
                ]
            )
            ->add('agreement', CheckboxType::class, [
                    'label' => 'registration.label.agreement',
                    'required' => true,
                    'mapped' => false,
                    'constraints' => [new IsTrue()]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $optionsResolver)
    {
        $optionsResolver->setDefaults([
            'translation_domain' => 'messages'
        ]);
    }
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}