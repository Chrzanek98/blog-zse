<?php

namespace KCMB\AppBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use KCMB\AppBundle\Form\EditProfileType;
use KCMB\AppBundle\Form\Handler\EditProfileFormHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends Controller
{
    public function viewAction($id = null)
    {
        if ($id === null) {
            $this->denyAccessUnlessGranted(['ROLE_USER'], null, "Nie masz dostępu do tej strony");
            $id = $this->getUser()->getId();
        }
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('ApplicationSonataUserBundle:User')->find($id);

        if ($user === null) {
            throw new NotFoundHttpException(sprintf("Nie ma użytkownika o id %s", $id));
        }

        return $this->render("user/view.html.twig", [
            'user' => $user
        ]);
    }

    public function profileAction()
    {
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            throw new AccessDeniedException("Nie masz dostępu do tej strony");
        }

        return $this->render("user/view.html.twig", [
            'user' => $user
        ]);
    }

    public function editAction(EditProfileFormHandler $formHandler, Session $session)
    {
        $user = $this->getUser();


        if (!$user instanceof UserInterface) {
            throw $this->createAccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->createForm(EditProfileType::class, $user);

        $isRequest = $formHandler->process($form, $user);

        if ($isRequest) {
            $session->getFlashBag()->add('success', 'Zakutalizowano profil');
            $form = $this->createForm(EditProfileType::class, $user);
        }

        return $this->render(':user:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}