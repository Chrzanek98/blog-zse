<?php

namespace KCMB\AppBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use KCMB\AppBundle\Entity\Comment;
use KCMB\AppBundle\Entity\Post;
use KCMB\AppBundle\Form\CommentFormType;
use KCMB\AppBundle\Form\Handler\CommentFormHandler;
use Sonata\UserBundle\Model\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\Translator;

class PostController extends Controller
{
    public function viewAction($id, Request $request,Session $session, Translator $translator, CommentFormHandler $commentFormHandler)
    {
        $em = $this->getDoctrine()->getRepository('KCMBAppBundle:Post');
        $post = $em->findOneBy([
            'id' => $id
        ]);
        $commentForm = $this->createForm(CommentFormType::class);
        $commentForm->handleRequest($request);
        if ($commentForm->isSubmitted()) {
            if ($commentForm->isValid()) {
                if (!($this->getUser() instanceof UserInterface) || $this->getUser() === null) {
                    throw new AccessDeniedException();
                }
                $data = $commentForm->getData();
                $comment = new Comment();
                $comment->setAuthor($this->getUser());
                $comment->setContent($data->getContent());
                $comment->setCreatedAt(new \DateTime("now"));
                $commentFormHandler->process($post, $comment);
                $session->getFlashBag()->add("success", $translator->trans('posts.form.success'));

                $commentForm = $this->createForm(CommentFormType::class);
            } else {
                $session->getFlashBag()->add("error", $translator->trans('posts.form.error'));
            }
        }
        return $this->render(':post:view.html.twig', [
            'post' => $post,
            'commentForm' => $commentForm->createView()
        ]);
    }

    public function addPostToFavouritesAction(Post $post)
    {
        /**
         * @var $user \Application\Sonata\UserBundle\Entity\User
         */
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            throw $this->createAccessDeniedException('This user does not have access to this section.');
        }
        $em = $this->getDoctrine()->getManager();

        $user->addFavouritePosts($post);

        $em->flush();

        return new JsonResponse("1", 200);
    }

    public function removePostFromFavouritesAction(Post $post)
    {
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            throw $this->createAccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();

        $user->removeFavouritePost($post);

        $em->flush();

        return new JsonResponse("1", 200);

    }

}