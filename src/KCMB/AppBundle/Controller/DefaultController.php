<?php

namespace KCMB\AppBundle\Controller;

use KCMB\AppBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    const PER_PAGE_LIMIT = 2;

    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }

    public function postsWidgetAction(Request $request)
    {
        $page = $request->get('page', 1);

        $em = $this->getDoctrine()->getRepository('KCMBAppBundle:Post');

        $posts = $em->findBy([], ['createdAt' => 'DESC'], self::PER_PAGE_LIMIT, ($page - 1) * self::PER_PAGE_LIMIT);

        $isLastPage = count($posts) < 2;

        return $this->render('default/postsWidget.html.twig', [
            'posts' => $posts,
            'isLastPage' => $isLastPage
        ]);
    }
}
