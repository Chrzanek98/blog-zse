<?php

namespace KCMB\AppBundle\Controller;


use KCMB\AppBundle\Form\ContactFormType;
use Sonata\UserBundle\Model\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class ContactController extends Controller
{
    public function viewAction(Request $request, Session $session)
    {
        $form = $this->createForm(ContactFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();
                if ($this->sendMail('contact/mail.html.twig', $data)) {
                    $session->getFlashBag()->add('success', 'Wiadomość została wysłana');
                }
                $form = $this->createForm(ContactFormType::class);
            } else {
                $session->getFlashBag()->add('error', 'Wystąpił problem przy wysyłaniu wiadomości');
            }
        }

        return $this->render('contact/view.html.twig', [
            'form' => $form->createView()
        ]);
    }

    private function sendMail($view, $data)
    {
        $mailer = $this->get('swiftmailer.mailer');

        $twig = $this->get('twig');

        $em = $this->getDoctrine()->getManager();
        /**
         * @var User[] $admins
         */
        $admins = $em
            ->getRepository('ApplicationSonataUserBundle:User')
            ->findByRole('ROLE_SUPER_ADMIN')
        ;

        $body = $twig->render($view, [
            'fullName' => $data['fullName'],
            'email' => $data['email'],
            'content' => $data['content']
        ]);
        foreach ($admins as $admin) {
            $message = \Swift_Message::newInstance()
                ->setSubject(sprintf("%s wysłał wiadomość za pomocą formuarza kontaktowego", $data['fullName']))
                ->setFrom($data['email'])
                ->setTo($admin->getEmail())
                ->setBody($body, 'text/html')
            ;
            if (!$mailer->send($message)) {
                return false;
            }
        }

        return true;
    }
}