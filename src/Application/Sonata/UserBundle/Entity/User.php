<?php

namespace Application\Sonata\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use KCMB\AppBundle\Entity\Post;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="UserRepository")
 * @ORM\Table(name="fos_user_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $nickname;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"all"})
     */
    protected $avatar;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $shortDescription;

    /**
     * @ORM\ManyToMany(targetEntity="KCMB\AppBundle\Entity\Post")
     */
    protected $favouritePosts;

    public function __construct()
    {
        parent::__construct();
        $this->favouritePosts = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param mixed $nickname
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    }

    /**
     * @return mixed
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @param mixed $shortDescription
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * @return mixed
     */
    public function getFavouritePosts() {
        return $this->favouritePosts;
    }

    /**
     * @param mixed $favouritePosts
     */
    public function setFavouritePosts($favouritePosts) {
        $this->favouritePosts = $favouritePosts;

    }

    public function addFavouritePosts(Post $post){
        if (!$this->favouritePosts->contains($post)) {
            $this->favouritePosts->add($post);
        }

        return $this;
    }

    public function removeFavouritePost(Post $favouritePost) {

        $this->favouritePosts->removeElement($favouritePost);

        return $this;
    }

}
