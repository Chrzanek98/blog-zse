<?php

require_once __DIR__.'/../vendor/autoload.php';


if (!is_file('composer.json')) {
    throw new \RuntimeException('Can\'t find a composer.json file. Make sure to start this script from the project root folder');
}

$rootDir = __DIR__.'/..';

use Symfony\Component\Console\Output\OutputInterface;

// reset data
$output = new \Symfony\Component\Console\Output\ConsoleOutput();

if (!is_file(__DIR__.'/../app/config/parameters.yml')) {
    $output->writeln('<error>no default apps/config/parameters.yml file</error>');
    exit(1);
}

/**
 * @param $commands
 * @param \Symfony\Component\Console\Output\ConsoleOutput $output
 *
 * @return bool
 */
function execute_commands($commands, $output)
{
    // find out the default php runtime
    $bin = sprintf('%s -d memory_limit=-1', defined('PHP_BINARY') ? PHP_BINARY : 'php');

    foreach ($commands as $command) {
        list($command, $message, $allowFailure) = $command;

        $output->write(sprintf(' - %\'.-70s', $message));
        $return = [];
        if (is_callable($command)) {
            $success = $command($output);
        } else {
            $p = new \Symfony\Component\Process\Process($bin . ' ' .$command);
            $p->setTimeout(null);
            $p->run(function ($type, $data) use (&$return) {
                $return[] = $data;
            });

            $success = $p->isSuccessful();
        }

        if (!$success && !$allowFailure) {
            $output->writeln('<error>KO</error>');
            $output->writeln(sprintf('<error>Fail to run: %s</error>', is_callable($command) ? '[closure]' : $command));
            foreach ($return as $data) {
                $output->write($data, false, OutputInterface::OUTPUT_RAW);
            }

            return false;
        } elseif (!$success) {
            $output->writeln('<info>!!</info>');
        } else {
            $output->writeln('<info>OK</info>');
        }
    }

    return true;
}


$output->writeln('<info>' . ($action) .' project...</info>');


if (extension_loaded('xdebug')) {
    $output->writeln('<error>WARNING, xdebug is enabled in the cli, this can drastically slowing down all PHP scripts</error>');
}

$maintanceFile = __DIR__ . '/../var/cache/maintenance';
touch($maintanceFile);

$success = execute_commands($commands, $output);

unlink($maintanceFile);

if (!$success) {
    $output->writeln('<info>An error occurs when running a command!</info>');

    exit(1);
}

exit(0);
