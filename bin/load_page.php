#!usr/bin/env php
<?php

require_once __DIR__.'/../vendor/autoload.php';

/*
 * This file is part of the Sonata package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
if (!is_file('composer.json')) {
    throw new \RuntimeException('Can\'t find a composer.json file. Make sure to start this script from the project root folder');
}
$rootDir = __DIR__ . '/..';

use Symfony\Component\Console\Output\OutputInterface;

$output = new \Symfony\Component\Console\Output\ConsoleOutput();

$output->writeln('<info>Sample output</info>');

exit(0);