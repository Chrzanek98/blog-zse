var KCMB = KCMB || {};

KCMB.post = function () {
    var currentPage = 1;
    var url = '';

    var init = function (targetUrl) {
        url = targetUrl;
        var $body = $("body");

        $(document).on({
            ajaxStart: function() { $body.addClass("loading");    },
            ajaxStop: function() { $body.removeClass("loading"); }
        });
        $('#load-more-button').on('click', function (e) {
            loadMore()
        });
    };

    var toggleFavourite = function(addUrl, removeUrl) {
        $('[data-rel=favourite]').on('click', function (e) {
            var $element = $(this);
            var action = $element.data('action');
            var url = '';

            if (action === 'remove') {
                url = removeUrl.replace('__ID__', $element.data('id'));
            } else {
                url = addUrl.replace('__ID__', $element.data('id'));
            }
            e.preventDefault();
            $.ajax({
                url: url,
                success: function (response) {
                    if (action === 'add') {
                        $element.data('action', 'remove');
                        $element.addClass('voted');
                        $('#favouriteText').html(' Dodano do ulubionych')
                    } else {
                        $element.data('action', 'add');
                        $element.removeClass('voted');
                        $('#favouriteText').html(' Dodaj do ulubionych')
                    }
                }
            })
        });
    };
    var getXHR = function () {
        var $xhr = false;

        $xhr = $.ajax({
            method: 'GET',
            url: url + "?page=" + currentPage
        });

        return $xhr;
    };

    var loadMore = function () {
        currentPage++;

        var $xhr = getXHR();
        $xhr.done(function (response) {
            var $items = $(response);
            var $target = $('#ajax-post');
            $target.append($items);
        });
    };

    return {
        init: init,
        toggleFavourite: toggleFavourite,
    }
}();